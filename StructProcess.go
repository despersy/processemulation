package main

type Process struct {
	idProc       int
	SubProcesses []ProcessRow
}

type ProcessRow struct {
	IsVir   bool
	pageNum int
	data    byte
}
