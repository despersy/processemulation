package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"
)

func main() {
	// создание физической памяти
	memLen := GeneratePhysMem()
	// создание процессов
	processes := GenerateProcesses(memLen)
	PrintProcesses(processes)
	fmt.Print("\nstart working\n")
	// создаем случайные обращения к памяти
	for i := 0; i < memLen; i++ {
		choosen := rand.Intn(len(processes)-0) + 0
		processes[choosen] = GetChoosenSub(processes[choosen])
	}
}

// выбор случайного процесса и обращения
func GetChoosenSub(proc Process) Process {
	choosenSub := rand.Intn(len(proc.SubProcesses)-0) + 0
	if !(proc.SubProcesses[choosenSub].IsVir) {
		data, e := FindPhysMem(proc.SubProcesses[choosenSub].pageNum)
		CheckErr(e)
		proc.SubProcesses[choosenSub].data = data
	}
	if proc.SubProcesses[choosenSub].pageNum != -1 {
		fmt.Printf("\nin process = %s subprocess id node = %s was satisfied and received data = %s", intToStr(proc.idProc), intToStr(proc.SubProcesses[choosenSub].pageNum), uint8ToStr(proc.SubProcesses[choosenSub].data))
	} else {
		fmt.Printf("\nprocess = %s using his virtual memory", intToStr(proc.idProc))
	}

	return proc
}

func PrintProcesses(procs []Process) {
	fmt.Println("list of processes")
	for i := 0; i < len(procs); i++ {
		fmt.Printf("\nProcess %s with subs:", procs[i].idProc)
		for j := 0; j < len(procs[i].SubProcesses); j++ {
			if procs[i].SubProcesses[j].IsVir {
				fmt.Printf("\n	sub %s is using virtual memory", intToStr(j))
			} else {
				fmt.Printf("\n	sub %s is calling for page %s", intToStr(j), intToStr(procs[i].SubProcesses[j].pageNum))
			}
		}
	}
}

// найти в физ памяти
func FindPhysMem(row int) (byte, error) {
	hddData := ReadCsv()
	for i := 0; i < len(hddData); i++ {
		if hddData[i][0] != "id" {
			if strToiInt(hddData[i][0]) == row {
				return byte(strToiInt(hddData[i][1])), nil
			}
		}
	}
	return 0, errors.New("Cant find in main memory")
}

// чтение файла физ памяти
func ReadCsv() [][]string {
	file, err := os.Open("hdd.csv")
	if err != nil {
		fmt.Println(err)
	}
	reader := csv.NewReader(file)
	records, _ := reader.ReadAll()
	return records
}

// создание процессов
func GenerateProcesses(memLen int) []Process {
	procs := []Process{}
	for i := 0; i < memLen/2; i++ {
		proc := Process{}
		proc.idProc = i

		for j := 0; j < 4; j++ {
			isv := RandBool()
			procOne := ProcessRow{}
			if isv {
				procOne = ProcessRow{
					IsVir:   true,
					pageNum: -1,
					data:    0,
				}
			} else {
				procOne = ProcessRow{
					IsVir:   false,
					pageNum: rand.Intn(memLen-0) + 0,
					data:    0,
				}
			}
			proc.SubProcesses = append(proc.SubProcesses, procOne)
		}
		procs = append(procs, proc)
	}
	return procs
}

// генерирование случайных данных в физ памяти
func GeneratePhysMem() int {
	f, e := os.Create("hdd.csv")
	CheckErr(e)

	writer := csv.NewWriter(f)
	var data = [][]string{
		{"id", "data"},
	}
	physMem := rand.Intn(10-5) + 5
	CheckErr(e)
	for i := 0; i < physMem; i++ {
		data = append(data, []string{intToStr(i), intToStr(rand.Intn(1000-100) + 100)})
	}

	e = writer.WriteAll(data)
	CheckErr(e)
	return physMem
}

// для проверки на ошибки
func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

// случайное определение для виртуальной памяти, либо обращения к физ памяти
func RandBool() bool {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(2) == 1
}
